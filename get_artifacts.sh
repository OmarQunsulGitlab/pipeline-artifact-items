source set_variables.sh

rm downloads/cross_join_errors_*.ndjson
rm downloads/cross_join_errors.ndjson
rm views/error_*.html

while read job_id; do
  curl --location --header "PRIVATE-TOKEN: $TOKEN" "https://$HOST/api/v4/projects/$PROJECT_ID/jobs/$job_id/artifacts/rspec/cross_join_errors.ndjson" > downloads/cross_join_errors_$job_id.ndjson
done < downloads/job_ids.txt

cat downloads/cross_join_errors_*.ndjson > downloads/cross_join_errors.ndjson
