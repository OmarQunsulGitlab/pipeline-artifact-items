source set_variables.sh

rm downloads/job_ids.txt

for page in {0..7}
do
  curl --header "PRIVATE-TOKEN: $TOKEN" "https://$HOST/api/v4/projects/$PROJECT_ID/pipelines/$PIPELINE_ID/jobs?scope[]=failed&per_page=$PAGE_SIZE&page=$page"|jq '.[]|select(.name | test("rspec"))|.id' >> downloads/job_ids.txt
done

