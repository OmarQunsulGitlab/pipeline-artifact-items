## What does this do?

TODO

## How to use?

1. Set the parameters in set_variables.sh
2. Call get_jobs.sh
3. Call get_artifacts.sh
4. Run `ruby parse.rb'
5. Browser the generated index.html in views folder


## Rails Code to write to the Artifacts

```
    if (%w[users namespaces] - tables).empty?
        open('rspec/cross_join_errors.ndjson', 'a') do |file| # this file will be in the tests artifacts
        app_backtrace = caller.grep(%r{/app/|/gitlab/lib/})
        spec_backtrace = caller.grep(%r{_spec\.rb})
        line = sql.match(%r{line:([^*]+)})[1]
        # Hashing the item to deduplicate them
        sha256 = OpenSSL::Digest.new('SHA256')
        sha256.update app_backtrace.inspect # rubocop:disable Rails/SaveBang
        hash = sha256.hexdigest
        item = {
            job: ENV["CI_JOB_NAME"],
            hash: hash,
            query: sql,
            line: line,
            app_backtrace: app_backtrace,
            spec_backtrace: spec_backtrace
        }
        file.puts(item.to_json)
        end
    end
```