require 'json'
require 'set'

items = {}

current_item = {}

File.open("downloads/cross_join_errors.ndjson", "r").each do |line|
  item = JSON.parse(line)
  hash = item.delete("hash")
  items[hash] = item unless items[hash]
end

items.each do |hash, item|
  app_backtrace = item["app_backtrace"]
  [/views/, /services/, /finders/, /concerns/, /models/, /controller/].each do |regex|
    if line = app_backtrace.grep(regex).first
      item["application_location"] = line
      break
    end
  end
  if item["application_location"].nil?
    puts item["app_backtrace"].inspect 
    item["application_location"] = "NA"
  end
end

index = "<html><title>Cross Join Errors</title><body>"
items.each do |hash, item|

  # In case you want to skip some items
  #next if item["line"] == "/lib/banzai/filter/references/user_reference_filter.rb:70:in `namespaces'"
  #next if item["line"] == "/app/models/group.rb:988:in `block in max_member_access'"
  #next if item["line"] == "/app/models/preloaders/user_max_access_level_in_groups_preloader.rb:40:in `preload_with_traversal_ids'"

  index += "<p>" + (item["job"] || " ") + "  :  <a href=\"error_#{hash}.html\">" +  item["application_location"] + "</a></p>"
  File.open("views/error_#{hash}.html", "w") do |f|
    f.puts "<html><body>"
    f.puts "<h2>Job</h2>"
    f.puts "<p>" + (item["job"] || "") + "</p>"
    f.puts "<h2>Line</h2>"
    f.puts "<p>" + (item["line"] || "") + "</p>"
    f.puts "<h2>Failing Test</h2>"
    item["spec_backtrace"].each do |line|
      f.puts "<p>" + line + "</p>"
    end
    f.puts "<h2>Query</h2>"
    f.puts "<p>" + item["query"] + "</p>"
    f.puts "<h2>Application Backtrace</h2>"
    item["app_backtrace"].each do |line|
      f.puts "<p>" + line + "</p>"
    end
    f.puts "</body></html>"
  end
end
index += "</body></html>"
File.open("views/index.html", "w") do |f|
  f.puts index
end





